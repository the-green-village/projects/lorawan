# -*- coding: utf-8 -*-
import json
import logging

import people_counters_data

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__file__)


# Global dictionary to store the latest MQTT messages.
# This dictionary is filled in the on_message function.
mqtt_values = {}


def on_connect(mqttc, obj, flags, rc):
    logger.info("Connected with result code " + str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    # water v3/tgv-watermeters@ttn/devices/#
    mqttc.subscribe("v3/+/devices/+/up", qos=0)


def on_disconnect(mqttc, obj, rc):
    if rc != 0:
        logger.warning("Unexpected disconnection.")


def on_message(mqttc, obj, msg):
    logger.debug(
        "Received message '"
        + str(msg.payload)
        + "' on topic '"
        + msg.topic
        + "' with QoS "
        + str(msg.qos)
    )
    if msg.retain==1:
        logger.debug("This is a retained message.")

    msg_json = json.loads(str(msg.payload.decode("utf-8")))

    # an example of a topic is:  v3/peoplecounters@ttn/devices/eui-0004a30b00f7857e-schuifhek/up
    if msg_json["end_device_ids"]["device_id"] in people_counters_data.devices.keys():
        if not (msg_json["uplink_message"]["frm_payload"] is None):
            # Add the value to the global dictionary for MQTT messages.
            # Previous values for every topic are overwritten.
            
            # here, the translation between decoded_payload and a functional unit should be made 
            logger.debug(msg_json["uplink_message"]["decoded_payload"])
            # Measurements
            mqtt_values["location"] = msg_json["uplink_message"]["rx_metadata"][0]["location"]
            mqtt_values["payload"] = msg_json["uplink_message"]["decoded_payload"]
            mqtt_values["timestamp"] = msg_json["received_at"]
            mqtt_values["device"] = msg_json["end_device_ids"]["device_id"]
            #mqtt_values["measurements"] = measurements

def on_publish(mqttc, obj, mid):
    logger.info("Messaged ID: " + str(mid))


def on_subscribe(mqttc, obj, mid, granted_qos):
    logger.info("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    logger.log(level, string)
