# -*- coding: utf-8 -*-
import json
import logging
import pathlib
import time
import datetime as dt

import mqtt_helper
import people_counters_data
import paho.mqtt.client as mqtt
from projectsecrets import MQTT_HOST, MQTT_PORT, MQTT_USERNAME, MQTT_PASSWORD, TOPIC 
from tgvfunctions import tgvfunctions

if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=logging.INFO,
    )
    logger = logging.getLogger(__file__)

    # Create tgv object and producer 
    tgv = tgvfunctions.TGVFunctions(TOPIC)
    producer = tgv.make_producer('lorawan_people_counters-producer')

    # MQTT client
    mqttc = mqtt.Client()
    mqttc.enable_logger(logger)

    mqttc.on_message = mqtt_helper.on_message
    mqttc.on_connect = mqtt_helper.on_connect
    mqttc.on_publish = mqtt_helper.on_publish
    mqttc.on_subscribe = mqtt_helper.on_subscribe

    mqttc.username_pw_set(MQTT_USERNAME, password=MQTT_PASSWORD)
    mqttc.connect(MQTT_HOST, MQTT_PORT, 60)
    mqttc.loop_start()

    try:
        while True:
            # Initialize empty dictionaries for the values section in the messages
            # in the generic schema format for each device.
            data_values = {device: [] for device in people_counters_data.device_list}

            # Current time in milliseconds
            # TODO get exact timestamp from mqtt message
            if len( mqtt_helper.mqtt_values )>0:            
                timestamp = int(time.time() * 1e3)
                
                # Turn the values received in MQTT messages into the generic schema format.
                payload = mqtt_helper.mqtt_values["payload"]
                device  = mqtt_helper.mqtt_values["device"]
                location= mqtt_helper.mqtt_values["location"]

                message = people_counters_data.devices[device]
                message["project_id"] = "People_Counters"
                message["latitude"] = location["latitude"]
                message["longitude"]= location["longitude"]
                message["timestamp"] = timestamp
                message["measurements"] = [
                    {
                        "measurement_id": "Battery Voltage",
                        "measurement_description": "battery_voltage",
                        "value": float(payload["battery_voltage"]),
                    },
                    {
                        "measurement_id": "Exit Terrain",
                        "measurement_description": "counter_a",
                        "value": payload["counter_a"],
                    },
                    {
                        "measurement_id": "Enter Terrain",
                        "measurement_description": "counter_b",
                        "value": payload["counter_b"],
                    },
                    {
                        "measurement_id": "Exit Terrain (total)",
                        "measurement_description": "total_counter_a",
                        "value": payload["total_counter_a"],
                    },
                    {
                        "measurement_id": "Enter Terrain (total)",
                        "measurement_description": "total_counter_b",
                        "value": payload["total_counter_b"],
                    },
                ] 
                logger.debug(json.dumps(message, sort_keys=True, indent=4))

                tgv.produce(producer, message)

            # Empty the global dictionary for MQTT messages.
            mqtt_helper.mqtt_values = {}

            # Trigger any available delivery report callbacks from previous produce() call.
            producer.poll(0)

            # Sleep for a bit so we don't flood the cluster.
            time.sleep(1)

    except KeyboardInterrupt:
        logger.error("Aborted by user")

    finally:
        producer.flush()
