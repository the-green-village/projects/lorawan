# -*- coding: utf-8 -*-
# Devices
device_list = [
    "eui-0004a30b00f7ceaa-loopbrug", 
    "eui-0004a30b00f344fa-grote-poort",
    "eui-0004a30b00f7857e-schuifhek"
]


# project_id:     energy?       water_usage        Lorawan?
# application_id: water_usage_ccc?  co_creation_centre water_meter_ccc?
# device_id:      water_meter
# measurement_id: meter_reading
# unit:           m3
# project_descr.: energy flows   -    Measure water usage of different buildings     - lora
# application_d.: ? from ttn:  application_id: tgv-watermeters 
# device_descr. : ? device_id: "co-creation-centre" 
# measurement_d.: ? DevEUI: 90DFFB818A2A404D
# N.B. link descriptions to message from ttn?

# Devices
devices = {}

devices["eui-0004a30b00f7ceaa-loopbrug"] = {
    "application_id": "People Counter Loopbrug",
    "device_id": "People Counter Loopbrug",
    "device_description": "eui-0004a30b00f7ceaa-loopbrug",
    "device_manufacturer": "imbuildings",
    "device_type": "People Counter LoRaWAN",
    "location_id": "Loopbrug",
}

devices["eui-0004a30b00f344fa-grote-poort"] = {
    "application_id": "People Counter Grote Poort",
    "device_id": "People Counter Grote Poort",
    "device_description": "eui-0004a30b00f344fa-grote-poort",
    "device_manufacturer": "imbuildings",
    "device_type": "People Counter LoRaWAN",
    "location_id": "Grote Poort",
}

devices["eui-0004a30b00f7857e-schuifhek"] = {
    "application_id": "People Counter Schuifhek",
    "device_id": "People Counter Schuifhek",
    "device_description": "eui-0004a30b00f7857e-schuifhek",
    "device_manufacturer": "imbuildings",
    "device_type": "People Counter LoRaWAN",
    "location_id": "Schuifhek",
}

