# People Counters (MQTT)

This script reads out data from people counters that are connected to The Things Network via LoRaWAN. 
When a new application is created on The Things Network, an MQTT server is automatically created with this application. 

To read messages from an application on TTN do:

```
mosquitto_sub -v -h eu1.cloud.thethings.network -t 'v3/+/devices/+/up' -u $MQTT_USERNAME -P $MQTT_PASSWORD
```

